package fr.cnam.flipme.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.cnam.flipme.R;
import fr.cnam.flipme.classes.DatabaseHelper;

public class ProjectAdapter extends BaseAdapter {
    private final String TAG = "ProjectAdapter";
    private Context context;
    private ArrayList<Integer> project_id;
    private ArrayList<String> project_name;
    private LayoutInflater inflater;

    public ProjectAdapter(Context context, ArrayList<Integer> project_id, ArrayList<String> project_name){
        this.context = context;
        this.project_id = project_id;
        this.project_name = project_name;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return project_id.size();
    }

    @Override
    public String getItem(int i) {
        return project_name.get(i);
    }

    @Override
    public long getItemId(int i) {
        return project_id.get(i);
    }

    // our ViewHolder.
    // caches our TextView
    static class ViewHolderItem {
        TextView project_id;
        TextView project_name;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolderItem viewHolder;

        if (view==null) {
            // Permet d'injecter le nombre d'élément dans notre liste dans notre ListView
            // Quand c'est la premiere fois qu'on charge notre vue, sachant que view==null
            view = inflater.inflate(R.layout.adapter_project, null);

            viewHolder = new ViewHolderItem();
            viewHolder.project_id = (TextView) view.findViewById(R.id.adapter_project_id_txt);
            viewHolder.project_name = (TextView) view.findViewById(R.id.adapter_project_name_txt);

            // On stock le contenu de notre view dans viewHolder
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) view.getTag();
        }

        // On récupère l'élément et on ajoute le nom du project
        long projectId = getItemId(i);
        String projectName = (String) getItem(i);

        if (!projectName.isEmpty()){
            viewHolder.project_id.setText(String.valueOf(projectId));
            viewHolder.project_name.setText(projectName);
        }

        return view;

    }


}
