package fr.cnam.flipme.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.cnam.flipme.R;
import fr.cnam.flipme.activities.ProjectActivity;
import fr.cnam.flipme.classes.DatabaseHelper;

public class TaskAdapter extends BaseAdapter {
    private final String TAG = "TaskAdapter";
    private Context context;
    private ArrayList<Integer> task_id, project_id;
    private ArrayList<String> task_name, task_time;
    private LayoutInflater inflater;

    public TaskAdapter(
            Context context,
            ArrayList<Integer> task_id,
            ArrayList<Integer> project_id,
            ArrayList<String> task_name,
            ArrayList<String> task_time
    ){
        this.context = context;
        this.task_id = task_id;
        this.project_id = project_id;
        this.task_name = task_name;
        this.task_time = task_time;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
       return task_id.size();
    }

    @Override
    public String getItem(int i) {
        return task_name.get(i);
    }
    public String getItemName(int i) {
        return task_name.get(i);
    }

    public String getItemTime(int i){
        return task_time.get(i);
    }

    @Override
    public long getItemId(int i) {
        return task_id.get(i);
    }

    // our ViewHolder.
    // caches our TextView
    static class ViewHolderItem {
        TextView task_id;
        TextView task_name;
        TextView task_time;
        ImageButton delete_btn;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderItem viewHolder;

        if (view==null) {
            // Permet d'injecter le nombre d'élément dans notre liste dans notre ListView
            // Quand c'est la premiere fois qu'on charge notre vue, sachant que view==null
            view = inflater.inflate(R.layout.adapter_task, null);
            viewHolder = new ViewHolderItem();
            viewHolder.task_id = view.findViewById(R.id.adapter_task_id);
            viewHolder.task_name = view.findViewById(R.id.adapter_task_name);
            viewHolder.task_time = view.findViewById(R.id.adapter_task_time);
            viewHolder.delete_btn = view.findViewById(R.id.adapter_task_delete_btn);

            // On stock le contenu de notre view dans viewHolder
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderItem) view.getTag();
        }

        // On récupère l'élément et on ajoute le nom de la tache
        long taskId = getItemId(i);
        String taskName = getItemName(i);
        String taskTime = getItemTime(i);

        Log.d(TAG, "task: " + taskName);
        if (!taskName.isEmpty()){
            viewHolder.task_id.setText(String.valueOf(taskId));
            viewHolder.task_name.setText(taskName);
            viewHolder.task_time.setText(taskTime);
        }

        viewHolder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper db = new DatabaseHelper(context);
                db.deleteTask(String.valueOf(taskId));

                // Refresh activity from here.
                ((ProjectActivity) context).recreate();;
            }
        });

        return view;
    }

}
