package fr.cnam.flipme.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import fr.cnam.flipme.R;
import fr.cnam.flipme.adapters.ProjectAdapter;
import fr.cnam.flipme.classes.DatabaseHelper;

public class ListProjectsActivity extends AppCompatActivity {
    private final String TAG = "ListProjectsActivity";
    private ListView projectsListView;
    private ProjectAdapter projectAdapter;
    private AlertDialog.Builder dialogProjectBuilder;
    private DatabaseHelper db;
    private ArrayList<Integer> project_id;
    private ArrayList<String> project_name;
    private FloatingActionButton addProjectBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_projects);

        // Récupérer la liste de taches
        db = new DatabaseHelper(getApplicationContext());
        project_id = new ArrayList<>();
        project_name = new ArrayList<>();

        retrieveProjectDataFromDatabase();

        // On récupère la ListView
        projectsListView = findViewById(R.id.list_projects_view);
        // L'adapter ici permet d'associer les éléments à notre listView
        projectAdapter = new ProjectAdapter(this, project_id, project_name);
        projectsListView.setAdapter(projectAdapter);

        projectsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent projectActivity = new Intent(ListProjectsActivity.this, ProjectActivity.class);
                projectActivity.putExtra("project_id", String.valueOf(projectAdapter.getItemId(i)));
                projectActivity.putExtra("project_name", projectAdapter.getItem(i));
                startActivity(projectActivity);
            }
        });

        buildingNewProjectDialog();

        // New Project
        addProjectBtn = findViewById(R.id.add_project_btn);
        addProjectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogProjectBuilder.show();
            }
        });
    }
    private void buildingNewProjectDialog(){
        dialogProjectBuilder = new AlertDialog.Builder(ListProjectsActivity.this);
        dialogProjectBuilder.setTitle("Add a new project");
        final EditText nameProjectInput = new EditText(ListProjectsActivity.this);

        dialogProjectBuilder.setView(nameProjectInput);
        dialogProjectBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String textValue = nameProjectInput.getText().toString();
                DatabaseHelper db = new DatabaseHelper(ListProjectsActivity.this);
                db.addProject(textValue);

                // Je supprime la vue pour cliquer plusieurs fois sur le boutton ajouter.
                ((ViewGroup) nameProjectInput.getParent()).removeView(nameProjectInput);

                // Je rafraichis la page pour afficher le nouveau projet.
                startActivity(getIntent());
            }
        });

        dialogProjectBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Je supprime la vue pour cliquer plusieurs fois sur le boutton ajouter.
                ((ViewGroup) nameProjectInput.getParent()).removeView(nameProjectInput);

                // CLOSE WINDOW.
            }
        });
    }
    private void retrieveProjectDataFromDatabase() {
        Cursor cursor = db.listProjects();
        if (cursor.getCount() == 0){
            Toast.makeText(this, "There is no project yet.", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                project_id.add(cursor.getInt(0));
                project_name.add(cursor.getString(1));
            }
        }
    }

}