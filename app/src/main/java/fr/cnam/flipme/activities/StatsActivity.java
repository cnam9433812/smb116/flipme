package fr.cnam.flipme.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import fr.cnam.flipme.R;
import fr.cnam.flipme.classes.ChronometerClass;
import fr.cnam.flipme.classes.DatabaseHelper;

public class StatsActivity extends AppCompatActivity {
    private final static String TAG = "StatsActivity";
    private final static Integer ONE_SECOND_IN_MILLISECONDS = 1_000;
    private ArrayList<String> xAxisProjectsNameEntries;
    private DatabaseHelper db;
    private ArrayList<Integer> project_id;
    private ArrayList<String> project_name, task_time;
    private HashMap<String, Long> projectsHash = new HashMap<String, Long>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        BarChart barChart = findViewById(R.id.chart);

        // ------------------------------- //
        // ---------- Database ----------- //
        // ------------------------------- //

        // Retrieve datas
        // First all project names
        db = new DatabaseHelper(getApplicationContext());
        project_id = new ArrayList<>();
        project_name = new ArrayList<>();
        retrieveProjectDataFromDatabase();

        // Pour chaque projet on ajoute dans notre HashMap {project_id: temps_total}
        projectsHash = new HashMap<String, Long>();
        for (int i = 0; i < project_id.size(); i++) {
            // using float as value to fit with BarEntry Y type below.
            projectsHash.put(
                    project_name.get(i),
                    getAllTimeOnProjectinMilliseconds(project_id.get(i))
            );
        }
        Log.d(TAG, "projectHash: " + projectsHash);

        // Feed our entries with data retrieved from DB.
        int index = 0;
        ArrayList <BarEntry>  entries = new ArrayList<>();
        xAxisProjectsNameEntries = new ArrayList<>();
        for (Map.Entry<String, Long> entry : projectsHash.entrySet()){
            String projectName = entry.getKey();
            Long totalTimeInMill = entry.getValue();

            xAxisProjectsNameEntries.add(projectName);
            // Format time in milliseconds to fit with minutes.seconds float Y axis.
            entries.add(new BarEntry(index, getMillisecondsTimeInSeconds(totalTimeInMill)));
            index += 1;
        }

        // ------------------------------- //
        // ---------- Charts UI ---------- //
        // ------------------------------- //

        // Right side
        barChart.getAxisRight().setDrawLabels(false); // No need to see the right axis numbers.

        // Left side
        barChart.getAxisLeft().setAxisMaximum(60); // 60 for 1 minute just for the example.
        barChart.getAxisLeft().setAxisMinimum(0);
        barChart.getAxisLeft().setAxisLineColor(Color.BLACK);
        barChart.getAxisLeft().setAxisLineWidth(2);
        barChart.getAxisLeft().setLabelCount(6); // to show every ten seconds in Y axis.

        // Add entries
        BarDataSet dataSet = new BarDataSet(entries, "projects");
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        BarData barData = new BarData(dataSet);
        barChart.setData(barData);

        // Custom charts
        barChart.getDescription().setEnabled(false);
        barChart.invalidate();

        barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisProjectsNameEntries));
        barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getXAxis().setGranularity(1f);
        barChart.getXAxis().setGranularityEnabled(true);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.animateY(2000);
    }
    private void retrieveProjectDataFromDatabase() {
        Cursor cursor = db.listProjects();
        if (cursor.getCount() == 0){
            Toast.makeText(this, "There is no projects yet in database.", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                project_id.add(cursor.getInt(0));
                project_name.add(cursor.getString(1));
            }
        }
    }
    private long getAllTimeOnProjectinMilliseconds(Integer projectId) {
        task_time = new ArrayList<>();
        Cursor cursor = db.getTasksFromProjectId(projectId);
        if (cursor.getCount() == 0){
            Toast.makeText(this, "A project has no tasks yet.", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                task_time.add(cursor.getString(2));
            }
        }

        // Format String in long
        long result = 0;
        for (String time : task_time){
            result += ChronometerClass.formatStringToMilliSeconds(time);
        }
        return result;
    }
    private Float getMillisecondsTimeInSeconds(long time){
        // Format time in milliseconds to fit with seconds float Y axis.
        return time / Float.valueOf(ONE_SECOND_IN_MILLISECONDS);
    }
}