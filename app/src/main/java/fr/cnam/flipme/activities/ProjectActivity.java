package fr.cnam.flipme.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import fr.cnam.flipme.R;
import fr.cnam.flipme.adapters.TaskAdapter;
import fr.cnam.flipme.classes.ChronometerClass;
import fr.cnam.flipme.classes.DatabaseHelper;


public class ProjectActivity extends AppCompatActivity implements SensorEventListener {
    private final String TAG = "ProjectActivity";
    private String projectId, projectName, taskTime;
    private ListView tasksListView;
    private TaskAdapter taskAdapter;
    private DatabaseHelper db;
    private ArrayList<Integer> task_id;
    private ArrayList<Integer> project_id;
    private ArrayList<String> task_name;
    private ArrayList<String> task_time;
    private Button startBtn, pauseBtn, doneBtn;
    private Chronometer chronometer;
    private ChronometerClass chronometerClass;
    private AlertDialog.Builder dialogNewTaskBuilder;
    private SensorManager sensorManager;
    private boolean semaphore;
    private final static int COUNTER_FLASH_NUMBER = 3;
    private int counterFlash = COUNTER_FLASH_NUMBER;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);

        // ------------------------------- //
        // ----------- Intent ------------ //
        // ------------------------------- //
        projectId = (String) getIntent().getSerializableExtra("project_id");
        projectName = (String) getIntent().getSerializableExtra("project_name");

        // Permet de modifier android:label (Manifest) dynamiquement pour cette vue, et
        // d'avoir le nom du projet en titre de l'activité.
        setTitle(projectName);

        // ------------------------------- //
        // ---------- Database ----------- //
        // ------------------------------- //
        db = new DatabaseHelper(getApplicationContext());
        task_id = new ArrayList<>();
        project_id = new ArrayList<>();
        task_name = new ArrayList<>();
        task_time = new ArrayList<>();
        retrieveTaskDataFromDatabase(Integer.valueOf(projectId));

        // ------------------------------- //
        // ---------- List view ---------- //
        // ------------------------------- //
        tasksListView = findViewById(R.id.listview_tasks);

        // On gère l'adapter pour l'inflate à notre listview.
        taskAdapter = new TaskAdapter(this, task_id, project_id, task_name, task_time);
        tasksListView.setAdapter(taskAdapter);

        // ------------------------------- //
        // ---------- Compteur ----------- //
        // ------------------------------- //
        chronometer = findViewById(R.id.chronometer);
        chronometerClass = new ChronometerClass(chronometer);

        startBtn = (Button) findViewById(R.id.start_btn);
        pauseBtn = (Button) findViewById(R.id.pause_btn);
        doneBtn = (Button) findViewById(R.id.done_btn);

        startBtn.setEnabled(true);
        pauseBtn.setEnabled(false);
        doneBtn.setEnabled(false);

        // Building dialog to add a new task when we stopped the chronometer.
        buildingNewTaskDialog();

        // Start timer
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startChronometer();
            }
        });

        // Pause timer
        pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pausedChronometer();
            }
        });

        // Reset timer
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneChronometer();
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        // ------------------------------- //
        // ----------- Sensor ------------ //
        // ------------------------------- //

        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        Sensor rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        semaphore = false;
    }
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Setup du menu, avec seulement notre bouton delete.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.project_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // On rend notre bouton delete cliquable.
        if(item.getItemId() == R.id.delete_project_btn_menu){
            confirmDeleteProjectDialog(projectName, projectId);
        }
        return super.onOptionsItemSelected(item);
    }
    public void confirmDeleteProjectDialog(String projectName, String projectId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProjectActivity.this);
        builder.setTitle(projectName);
        builder.setMessage("Are you sure to delete this project ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DatabaseHelper db = new DatabaseHelper(getApplicationContext());
                db.deleteProject(projectId);

                // Je passe par la creation d'un intent à la place de finish() pour que les modifications
                // soient prisent en compte.
                Intent intent = new Intent(ProjectActivity.this, ListProjectsActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // CLOSE.
            }
        });
        builder.create().show();
    }
    private void buildingNewTaskDialog(){
        dialogNewTaskBuilder = new AlertDialog.Builder(ProjectActivity.this);
        dialogNewTaskBuilder.setTitle("Saved this new task ?");
        final EditText nameTaskInput = new EditText(ProjectActivity.this);

        dialogNewTaskBuilder.setView(nameTaskInput);
        dialogNewTaskBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String taskName = nameTaskInput.getText().toString();
                DatabaseHelper db = new DatabaseHelper(ProjectActivity.this);
                db.addTask(taskName, taskTime, Integer.valueOf(projectId));

                // Je supprime la vue pour cliquer plusieurs fois sur le boutton ajouter.
                ((ViewGroup) nameTaskInput.getParent()).removeView(nameTaskInput);

                // Je rafraichis la page pour afficher le nouveau projet.
                startActivity(getIntent());
            }
        });
        dialogNewTaskBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Je supprime la vue pour cliquer plusieurs fois sur le boutton ajouter.
                ((ViewGroup) nameTaskInput.getParent()).removeView(nameTaskInput);

                // CLOSE WINDOW.
            }
        });
    }
    private void retrieveTaskDataFromDatabase(int projectId) {
        Cursor cursor = db.getTasksFromProjectId(projectId);
        if (cursor.getCount() == 0){
            Toast.makeText(this, "There is no task yet.", Toast.LENGTH_SHORT).show();
        } else {
            while (cursor.moveToNext()) {
                task_id.add(cursor.getInt(0));
                task_name.add(cursor.getString(1));
                task_time.add(cursor.getString(2));
                project_id.add(cursor.getInt(3));
            }
        }
    }
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float pitch = sensorEvent.values[1];

        // Pitch with value > 150 and value < -150 == phone returned.
        if (pitch > 150 || pitch < -150) {

            Log.d(TAG, "Phone faces down.");
            startChronometer();

            for (int i = 0; i < counterFlash; i++){
                flashlight();
                counterFlash -= 1;
            }
            semaphore = true;


        } if (semaphore == true && (pitch < 20 && pitch > -20)) {
            Log.d(TAG, "Phone faces up.");

            try {
               pausedChronometer();
               counterFlash = COUNTER_FLASH_NUMBER;
            } catch (Exception e) {
                Log.e(TAG, "Error: " + e);
            } finally {
                semaphore = false;
            }
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        Log.d(TAG, "SENSOR Accuracy Changed: " + sensor);
    }

    private void startChronometer(){
        Log.d(TAG, "Starting chronometer !");
        startBtn.setEnabled(false);
        pauseBtn.setEnabled(true);
        doneBtn.setEnabled(true);


        chronometerClass.startChronometer();
    }
    private void pausedChronometer(){
        Log.d(TAG, "Paused timer");
        startBtn.setEnabled(true);
        pauseBtn.setEnabled(false);
        doneBtn.setEnabled(true);

        chronometerClass.pauseChronometer();
    }

    private void doneChronometer(){
        Log.d(TAG, "Reseted timer");
        chronometerClass.doneChronometer();

        // taskTime is retrieve by dialog window to send result to database.
        taskTime = chronometerClass.formatTimeToString();

        // Enregistrer cette tache
        dialogNewTaskBuilder.show();

        // Do not forget to reset the chronometer after database insert.
        chronometerClass.resetChronometer();

        // reset interface.
        semaphore = false;
        startBtn.setEnabled(true);
        doneBtn.setEnabled(false);
    }

    private void flashlight(){

        if (this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)){
            Log.d(TAG, "Flashligth detected.");
            CameraManager cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
            try {
                // [0] is to retrieve the back camera.
                String cameraID = cameraManager.getCameraIdList()[0];
                cameraManager.setTorchMode(cameraID, true);
                TimeUnit.MILLISECONDS.sleep(500);
                cameraManager.setTorchMode(cameraID, false);
            } catch (CameraAccessException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else {
            Toast.makeText(this, "No flashlight on your phone.", Toast.LENGTH_SHORT).show();
        }
    }
}