package fr.cnam.flipme.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.compose.LottieCompositionSpec;

import java.util.Timer;
import java.util.TimerTask;

import fr.cnam.flipme.R;
import fr.cnam.flipme.classes.DatabaseHelper;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    private Button viewProjectsBtn, viewStatsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Statistic view
        viewStatsBtn = (Button) findViewById(R.id.statistic_btn);
        viewStatsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewStatsActivity = new Intent(MainActivity.this, StatsActivity.class);
                startActivity(viewStatsActivity);
            }
        });

        // View Projects
        viewProjectsBtn = (Button) findViewById(R.id.view_projects_btn);
        viewProjectsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewProjectsActivity = new Intent(MainActivity.this, ListProjectsActivity.class);
                startActivity(viewProjectsActivity);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Setup du menu, avec seulement notre bouton delete.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // On rend notre bouton delete cliquable.
        if(item.getItemId() == R.id.delete_all_btn_menu){
            DatabaseHelper db = new DatabaseHelper(getApplicationContext());
            db.deleteALlProjects();
            db.deleteALlTasks();
            Toast.makeText(this, "Reset database done.", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}