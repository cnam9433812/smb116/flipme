package fr.cnam.flipme.classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context;
    private static final String DATABASE_NAME = "Flipme.db";
    private static final int DATABASE_VERSION = 1;

    private static final String PROJECT_TABLE_NAME = "projects";
    private static final String PROJECT_COLUMN_ID = "_id";
    private static final String PROJECT_COLUMN_NAME = "name";

    private static final String TASK_TABLE_NAME = "tasks";
    private static final String TASK_COLUMN_ID = "_id";
    private static final String TASK_COLUMN_PROJECT_ID = "project_id";
    private static final String TASK_COLUMN_NAME = "name";
    private static final String TASK_COLUMN_TIME = "time";


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String project_query_table =
                "CREATE TABLE " + PROJECT_TABLE_NAME + " (" +
                        PROJECT_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        PROJECT_COLUMN_NAME + " TEXT NOT NULL);";

        String task_query_table = "CREATE TABLE " + TASK_TABLE_NAME + " (" +
                TASK_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TASK_COLUMN_NAME + " TEXT NOT NULL, " +
                TASK_COLUMN_TIME + " TEXT NOT NULL, " +
                TASK_COLUMN_PROJECT_ID + " INTEGER NOT NULL, " +
                "FOREIGN KEY (" + TASK_COLUMN_PROJECT_ID + ") " +
                "REFERENCES " + PROJECT_TABLE_NAME + "(" + PROJECT_COLUMN_ID + "));";

        db.execSQL(project_query_table);
        db.execSQL(task_query_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + PROJECT_TABLE_NAME + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TASK_TABLE_NAME + ";");
        onCreate(db);
    }

    // ----------------------------- //
    // ----------------------------- //
    // --------- Projets ----------- //
    // ----------------------------- //
    // ----------------------------- //

    public void addProject(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Les données de l'app seront passées à la base de donnée via ContentValues.
        ContentValues contentValues = new ContentValues();

        // key: values --> key: column_name ; values: data
        contentValues.put(PROJECT_COLUMN_NAME, name);

        long result = db.insert(PROJECT_TABLE_NAME, null, contentValues);
        if (result == -1){
            Toast.makeText(context, "Error during adding new project.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Project added with success !", Toast.LENGTH_SHORT).show();
        }
    }

    public Cursor listProjects() {
        String query = "SELECT * FROM " + PROJECT_TABLE_NAME + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        if (db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    public Cursor getProject(int projectId){
        String projectIdString = String.valueOf(projectId);
        String query = "SELECT * FROM " + PROJECT_TABLE_NAME +
                " WHERE " + PROJECT_COLUMN_ID + "=" + projectIdString + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        if (db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    public void updateProject(String projectId, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PROJECT_COLUMN_NAME, name);

        long result = db.update(PROJECT_TABLE_NAME, contentValues, "_id=?", new String[]{projectId});
        if (result == -1){
            Toast.makeText(context, "Updated project failed.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Updated project realised with success !", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteProject(String projectId){
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(PROJECT_TABLE_NAME, "_id=?", new String[]{projectId});
        if (result == -1){
            Toast.makeText(context, "Impossible to delete this project.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Deletion realised with success !", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteALlProjects(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + PROJECT_TABLE_NAME);
    }

    // ----------------------------- //
    // ----------------------------- //
    // ---------- Tasks ------------ //
    // ----------------------------- //
    // ----------------------------- //

    public Cursor listTasks() {
        String query = "SELECT * FROM " + TASK_TABLE_NAME + ";";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        if (db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }

    public Cursor getTasksFromProjectId(int projectId) {
        // Je recupere les taches par ordre descendant pour avoir les dernieres taches
        // en haut de ma listview.
        String query = "SELECT * FROM " + TASK_TABLE_NAME +
                " WHERE " + TASK_COLUMN_PROJECT_ID + "=" + projectId +
                " ORDER BY " + TASK_COLUMN_ID + " DESC;";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        if (db != null){
            cursor = db.rawQuery(query, null);
        }
        return cursor;
    }


    public void addTask(String name, String time, int projectId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TASK_COLUMN_NAME, name);
        contentValues.put(TASK_COLUMN_TIME, time);
        contentValues.put(TASK_COLUMN_PROJECT_ID, projectId);

        long result = db.insert(TASK_TABLE_NAME, null, contentValues);
        if (result == -1){
            Toast.makeText(context, "Error during adding new task.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Task added with success !", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteTask(String taskId){
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(TASK_TABLE_NAME, "_id=?", new String[]{taskId});
        if (result == -1){
            Toast.makeText(context, "Impossible to delete this task.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Task deleted with success !", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteALlTasks(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TASK_TABLE_NAME);
    }
}
