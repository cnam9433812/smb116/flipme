package fr.cnam.flipme.classes;

import android.os.SystemClock;
import android.util.Log;
import android.widget.Chronometer;

import java.text.MessageFormat;
import java.util.Date;
import java.util.TimeZone;

public class ChronometerClass {
    private final static String TAG = "ChronometerClass";
    private final static Integer ONE_HOUR_IN_MILLISECONDS = 3_600_000;
    private final static Integer ONE_MINUTE_IN_MILLISECONDS = 60_000;
    private final static Integer ONE_SECOND_IN_MILLISECONDS = 1_000;
    private Chronometer chronometer;
    private boolean running;
    private long timerTime;


    public ChronometerClass(Chronometer chronometer){
        this.chronometer = chronometer;
    }

    public void startChronometer(){
        if(!running){
            // Offset is here to start with the right timing if I paused my chronometer.
            chronometer.setBase(SystemClock.elapsedRealtime() - timerTime);
            chronometer.start();
            running = true;
        }
    }
    public void pauseChronometer(){
        if(running){
            // Stop updating the text but not the timer.
            chronometer.stop();
            // So I add an offset to calculate the difference between now and when I started
            // the chronometer.
            timerTime = SystemClock.elapsedRealtime() - chronometer.getBase();
            running = false;
        }
    }
    public void doneChronometer() {
        if (running){
            // To avoid to retrieve a 0 timer if I using Done button only without pause before.
            timerTime = SystemClock.elapsedRealtime() - chronometer.getBase();
        }
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.stop();
    }

    public void resetChronometer(){
        // I splitted reset and done to catch the correct timerTime to add it in databae.
        // Then reset our timer just after task database.
        running = false;
        timerTime = 0;
    }

    public String formatTimeToString(){
        // Convert milliseconds in hours:minutes:seconds format.
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Date date = new Date(timerTime);

        String result = MessageFormat.format(
                "{0}:{1}:{2}",
                String.valueOf(date.getHours()),
                String.valueOf(date.getMinutes()),
                String.valueOf(date.getSeconds())
        );

        return result;
    }

    public static long formatStringToMilliSeconds(String time){
        int hours, minutes, seconds;
        long result = 0;
        long hoursInMill = 0;
        long minutesInMill = 0;
        long secondsInMill = 0;

        String[] timeString = time.split(":");
        hours = Integer.valueOf(timeString[0]);
        minutes = Integer.valueOf(timeString[1]);
        seconds = Integer.valueOf(timeString[2]);

        if (hours != Integer.valueOf(0)){
            hoursInMill = hours * ONE_HOUR_IN_MILLISECONDS;
        }
        if (minutes != Integer.valueOf(0)){
            minutesInMill = minutes * ONE_MINUTE_IN_MILLISECONDS;
        }
        if (seconds != Integer.valueOf(0)){
            secondsInMill = seconds * ONE_SECOND_IN_MILLISECONDS;
        }

        result = hoursInMill + minutesInMill + secondsInMill;
        return result;
    }
}
